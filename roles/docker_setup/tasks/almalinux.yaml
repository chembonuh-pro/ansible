- name: Update dnf cache
  ansible.builtin.dnf:
    update_cache: true

- name: Ensure firewalld is installed
  ansible.builtin.yum:
    name: firewalld
    state: present

- name: Ensure firewalld is started and enabled
  ansible.builtin.systemd:
    name: firewalld
    state: started
    enabled: true

- name: Install dnf plugins
  ansible.builtin.dnf:
    name:
      - dnf-plugins-core
    state: present

- name: Add Docker repo
  ansible.builtin.command:
    cmd: dnf config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
  changed_when: false

- name: Install docker
  ansible.builtin.dnf:
    name:
      - docker-ce
      - docker-ce-cli
      - containerd.io
    state: present

- name: Install pip for Python package installation
  ansible.builtin.dnf:
    name: python3-pip
    state: present

- name: Install python-docker using pip
  ansible.builtin.pip:
    name: docker
    state: present

- name: Reload Docker service units
  ansible.builtin.systemd:
    daemon_reload: true
    name: docker
  become: true

- name: Start docker.service
  ansible.builtin.systemd:
    name: docker
    state: started

- name: Install loki plugin
  community.docker.docker_plugin:
    plugin_name: grafana/loki-docker-driver:latest
    alias: loki
    state: enable

- name: If the docker volumes are going to be stored on ZFS
  when: "ansible_mounts | selectattr('mount', 'equalto', '/tank/dockerdata') | list | count > 0"
  block:
    - name: Gather facts about ZFS dataset tank/dockerdata
      community.general.zfs_facts:
        dataset: tank
        recurse: true
        parsable: true
      register: docker_data_info
      changed_when: false

    - name: Print tank/dockerdata facts
      ansible.builtin.debug:
        var: docker_data_info
        verbosity: 1

    - name: Create ZFS dataset if it doesn't exist
      ansible.builtin.set_fact:
        dockerdata_exists: true
      when:
        - item.name == "tank/dockerdata"
      loop: "{{ docker_data_info.ansible_facts.ansible_zfs_datasets }}"

- name: Stop Docker Service
  ansible.builtin.systemd:
    name: docker.service
    state: stopped
  when:
    - not dockerdata_exists

- name: Create docker dataset folder
  ansible.builtin.file:
    path: /var/lib/docker
    state: directory
    mode: '0755'
  when:
    - not dockerdata_exists

# - name: Ensure the docker dataset is created
#   community.general.zfs:
#     name: tank/dockerdata
#     state: present
#     extra_zfs_properties:
#       mountpoint: /var/lib/docker
#   become: true
#   when:
#      - not dockerdata_exists

- name: Restart Docker Service
  ansible.builtin.systemd:
    name: docker.service
    state: started
  when:
    - not dockerdata_exists

- name: Configure daemon.json for insecure registries
  ansible.builtin.include_tasks: configure_daemon.yaml
