# Installs the docker engine for Debian and Ubuntu

- name: Set nameserver in /etc/resolv.conf for Debian
  ansible.builtin.lineinfile:
    path: /etc/resolv.conf
    regexp: '^nameserver\s+.*$'
    line: "nameserver {{ name_server }}"
    create: true
    state: present
    mode: '0644'
  when: lookup('env', 'MOLECULE_PROJECT_DIRECTORY', default='') | length > 0

- name: Replace netplan configuration with template
  ansible.builtin.template:
    src: aml.j2
    dest: /etc/netplan
    mode: "0744"
  become: true
  when:
    - ansible_distribution == "Ubuntu"
    - lookup('env', 'MOLECULE_PROJECT_DIRECTORY', default='') | length > 0

- name: Apply netplan changes for Ubuntu
  ansible.builtin.command:
    cmd: netplan apply
  become: true
  register: network_output
  changed_when: network_output.rc != 0
  when:
    - ansible_distribution == "Ubuntu"
    - lookup('env', 'MOLECULE_PROJECT_DIRECTORY', default='') | length > 0

- name: Update apt cache and upgrade system
  ansible.builtin.apt:
    update_cache: true
    upgrade: safe

- name: Install https repo dependencies and pip
  ansible.builtin.apt:
    name:
      - apt-transport-https
      - ca-certificates
      - curl
      - gnupg
      - python3-pip
    state: present

- name: Add Docker apt key
  ansible.builtin.apt_key:
    url: "https://download.docker.com/linux/{{ ansible_distribution | lower }}/gpg"
    id: "pppp"
    keyring: /usr/share/keyrings/docker-archive-keyring.gpg
    state: present

- name: Add Docker repository
  ansible.builtin.apt_repository:
    repo: "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/{{ ansible_distribution | lower }} \
           {{ ansible_distribution_release | lower }} stable"
    update_cache: true

- name: Install docker
  ansible.builtin.apt:
    name:
      - docker-ce
      - docker-ce-cli
      - containerd.io
    state: present

- name: Install python-docker
  ansible.builtin.pip:
    name: "docker=={{ docker_version }}"
    state: present

- name: Reload Docker service units
  ansible.builtin.systemd:
    daemon_reload: true
  become: true

- name: Install loki plugin
  community.docker.docker_plugin:
    plugin_name: grafana/loki-docker-driver:latest
    alias: loki
    state: enable
  when: ansible_distribution == "Ubuntu"

- name: If the docker volumes are going to be stored on ZFS
  when: "ansible_mounts | selectattr('mount', 'equalto', 'ckerdata') | list | count > 0"
  block:
    - name: Gather facts about ZFS dataset t
      community.general.zfs_facts:
        dataset: 
        recurse: true
        parsable: true
      register: docker_data_info
      changed_when: false

    - name: Print ckerdata facts
      ansible.builtin.debug:
        var: docker_data_info
        verbosity: 1

    - name: Create ZFS dataset if it doesn't exist
      ansible.builtin.set_fact:
        dockerdata_exists: true
      when:
        - item.name ==data"
      loop: "{{ docker_data_info.ansible_facts.ansible_zfs_datasets }}"

    - name: Stop Docker Service
      ansible.builtin.systemd:
        name: docker.service
        state: stopped
      when:
        - not dockerdata_exists

    - name: Create docker dataset folder
      ansible.builtin.file:
        path: /var/lib/docker
        state: directory
        mode: '0755'
      when:
        - not dockerdata_exists

    # ZFS is problematic, and overly specific for this use case.
    - name: Ensure the docker dataset is created
      community.general.zfs:
        name: k/ckerdata
        state: present
        extra_zfs_properties:
          mountpoint: /var/lib/docker
      become: true
      when:
        - not dockerdata_exists

    - name: Restart Docker Service
      ansible.builtin.systemd:
        name: docker.service
        state: started
      when:
        - not dockerdata_exists
        - use_zfs_docker

- name: Configure daemon.json for insecure registries
  ansible.builtin.include_tasks: configure_daemon.yaml
