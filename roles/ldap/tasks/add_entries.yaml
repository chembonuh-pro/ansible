# roles/docker_openldap/tasks/add_entries.yaml
---
- name: Copy memberof module ldif file
  ansible.builtin.copy:
    src: ./files/load_memberof.ldif
    dest: "{{ ldap_certs_volume_params.driver_options.device }}/load_memberof.ldif"
    mode: '0755'

- name: Load memberof module
  ansible.builtin.command:
    cmd: >
      docker exec openldap ldapadd -Y EXTERNAL -H ldapi:/// -f /opt/bitnami/openldap/certs/load_memberof.ldif
  register: module_output
  changed_when: module_output.rc != 0
  ignore_errors: true

- name: Ensure python-ldap is installed
  ansible.builtin.package:
    name: python3-ldap
    state: present

- name: Set The Schema
  ansible.builtin.set_fact:
    ldap_schema_entries: "{{ lookup('template', './ldap_entries.j2') | from_yaml }}"

- name: Debug The Schema
  ansible.builtin.debug:
    var: ldap_schema_entries

- name: Return current live schema Distinguished Names
  community.general.ldap_search:
    dn: "{{ base_dn }}"
    scope: "children"
    attrs:
      - "dn"
    server_uri: "ldaps://{{ ansible_host }}:{{ openldap_ports }}"
    bind_dn: "{{ bind_dn }}"
    bind_pw: "{{ ldap_admin_password }}"
    validate_certs: false
  register: ldap_live_schema

- name: Create Reject-List to speed past loops
  ansible.builtin.set_fact:
    rejected_list: "{{ ldap_live_schema.results | map(attribute='dn') | list }}"

- name: Add ldap OU entries
  community.general.ldap_entry:
    dn: "{{ item.dn }}"
    objectClass: "{{ item.objectClass }}"
    attributes: "{{ item.attributes }}"
    state: "{{ item.state }}"
    server_uri: "ldaps://{{ ldap_host }}:{{ openldap_ports }}"
    bind_dn: "{{ bind_dn }}"
    bind_pw: "{{ ldap_admin_password }}"
    validate_certs: false
  loop: "{{ ldap_schema_entries.organizational_ldapentries | rejectattr('dn', 'in', (rejected_list)) }}"

- name: Add users and groups
  community.general.ldap_entry:
    dn: "{{ item.dn }}"
    objectClass: "{{ item.objectClass }}"
    attributes: "{{ item.attributes }}"
    server_uri: "ldaps://{{ ldap_host }}:{{ openldap_ports }}"
    bind_dn: "{{ bind_dn }}"
    bind_pw: "{{ ldap_admin_password }}"
    state: "{{ item.state }}"
    validate_certs: false
  loop: "{{ (ldap_schema_entries.users_ldapentries + ldap_schema_entries.groups_ldapentries) | rejectattr('dn', 'in', (rejected_list)) }}"

- name: Add and Remove users within a group
  community.general.ldap_attrs:
    dn: "{{ item.dn }}"
    attributes:
      uniqueMember: >
        {{ (ldap_schema_entries.users_ldapentries | json_query(jquery)) + item.attributes.uniqueMember }}
    state: exact
    server_uri: "ldaps://{{ ansible_host }}:{{ openldap_ports }}"
    bind_dn: "{{ bind_dn }}"
    bind_pw: "{{ ldap_admin_password }}"
    validate_certs: false
  vars:
    jquery: "[?contains(ldap_groups, '{{ item.attributes.cn }}')].dn"
  loop: "{{ ldap_schema_entries.groups_ldapentries }}"
