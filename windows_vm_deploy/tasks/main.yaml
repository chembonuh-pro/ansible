---
- name: Include vm_provision role to install requirements
  ansible.builtin.include_role:
    name: vm_provision

- name: Install packages on  Ubuntu/Debian
  ansible.builtin.apt:
    name:
      - ruby-dev
      - ruby-libvirt
    state: present
  when: ansible_os_family == "Debian"

- name: Install packages on Redhat
  ansible.builtin.yum:
    name:
      - ruby-dev
      - ruby-libvirt
    state: present
  when: ansible_os_family == 'RedHat'


- name: Import HashiCorp GPG key
  ansible.builtin.get_url:
    url: https://apt.releases.hashicorp.com/gpg
    dest: /usr/share/keyrings/hashicorp-archive-keyring.gpg
    owner: root
    group: root
    mode: '0644'

- name: Add HashiCorp repository
  ansible.builtin.lineinfile:
    path: /etc/apt/sources.list.d/hashicorp.list
    line: "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com {{ ansible_distribution_release }} main"
    create: true
    owner: root
    group: root
    mode: '0644'

- name: Update apt cache on Debian/Ubuntu
  ansible.builtin.apt:
    update_cache: true
  when: ansible_os_family == "Debian"

- name: Update apt cache on Redhat
  ansible.builtin.yum:
    update_cache: true
  when: ansible_os_family == 'RedHat'

- name: Install vagrant on Ubuntu/Debian
  ansible.builtin.apt:
    name: vagrant
    state: present
    force: true
  when: ansible_os_family == "Debian"

- name: Install vagrant on Redhat
  ansible.builtin.yum:
    name: vagrant
    state: present
    force: true
  when: ansible_os_family == 'RedHat'

- name: Check if Vagrant-libvirt plugin is installed
  ansible.builtin.command:
    argv:
      - vagrant
      - plugin
      - list
  register: windows_vm_deploy_plugin_check
  changed_when: false
  failed_when: false

- name: Install Vagrant-libvirt plugin
  ansible.builtin.command:
    argv:
      - vagrant
      - plugin
      - install
      - vagrant-libvirt
  changed_when: plugin_check.rc != 0
  when: "'vagrant-libvirt' not in windows_vm_deploy_plugin_check.stdout"

- name: Check if Vagrant box exists
  ansible.builtin.command:
    argv:
      - vagrant
      - box
      - list
  register: windows_vm_deploy_box_check
  changed_when: false
  failed_when: false

- name: Download Vagrant box
  ansible.builtin.command:
    argv:
      - vagrant
      - box
      - add
      - peru/windows-10-enterprise-x64-eval
      - --provider
      - libvirt
  when: "'peru/windows-10-enterprise-x64-eval' not in windows_vm_deploy_box_check.stdout"
  changed_when: windows_vm_deploy_box_check.rc != 0

- name: Render Vagrantfile from template
  ansible.builtin.template:
    src: Vagrantfile.j2
    dest: /home/ansible/Vagrantfile
    owner: ansible
    mode: '0664'
  args:
    creates: /home/ansible/Vagrantfile

- name: Deploy Windows VM
  ansible.builtin.command:
    argv:
      - vagrant
      - up
      - --no-destroy-on-error
      - --provider=libvirt
  args:
    chdir: /home/ansible
    warn: true
  changed_when: false
  register: windows_vm_deploy_vagrant_result

- name: Check Vagrant deployment status
  ansible.builtin.debug:
    var: windows_vm_deploy_vagrant_result
